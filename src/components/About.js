import React from 'react';
import '../App.css';
import png from './2.png';


class About extends React.Component {
  render() {
    return(
      <div className="about" id="about">
        <div className="container mt-3 pb-3 con-about">
          <div className="row pt-5 mt-5 pl-5">
            <div className="col-md-12">
              <div className="overlay">
                <h1 className="h1-main text-right">Web&nbsp;Skill</h1>
              </div>
            </div>
          </div>

          <div className="row mt py">
            <div className="col-md-6 mt-0">
              <img className="img-fluid mb-3" src={png} alt="web designer"/>
            </div>
            <div className="col-md-6 mt-5">
              <h5 className="progress-label"><strong>HTML</strong></h5>
              <div className="progress">
                <div className="progress-bar bg-info" role="progressbar"  style={{width: "75%"}} aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">75%</div>
              </div>
              <br/>
              <h5 className="progress-label"><strong>CSS</strong></h5>
              <div className="progress">
                <div className="progress-bar bg-info" role="progressbar"  style={{width: "70%"}} aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">70%</div>
              </div>
              <br/>
              <h5 className="progress-label"><strong>Javascript</strong></h5>
              <div className="progress">
                <div className="progress-bar bg-info" role="progressbar"  style={{width: "68%"}} aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">68%</div>
              </div>
              <br/>
              <h5 className="progress-label"><strong>jQuery</strong></h5>
              <div className="progress">
                <div className="progress-bar bg-info" role="progressbar"  style={{width: "50%"}} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
              </div>
              <br/>
              <h5 className="progress-label"><strong>Bootstrap</strong></h5>
              <div className="progress">
                <div className="progress-bar bg-info" role="progressbar"  style={{width: "60%"}} aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%</div>
              </div>
              <br/>
              <h5 className="progress-label"><strong>React JS</strong></h5>
              <div className="progress">
                <div className="progress-bar bg-info" role="progressbar"  style={{width: "45%"}} aria-valuenow="45" aria-valuemin="0" aria-valuemax="100">45%</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default About;
