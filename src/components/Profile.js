import React from 'react';
import '../App.css';
import user from './law.jpg';

class Profile extends React.Component {
  render() {
    return(
      <div className="container-fluid con-profile mt" id="profile">
        <div className="row pt-3 mt-5">
          <div className="col-md-12">
            <div className="overlay">
              <h1 className="h1-main">Profile</h1>
            </div>
          </div>
        </div>

        <div className="row mt">
          <div className="col-md-2 ofset-md-4 mx-auto text-center">
            <img className="img fluid img-profile mb-3 mb-md-0" src={user} alt="Luthfi"/>
          </div>
          <div className="col-md-6">
            <p className="text-center hi">Hi!</p>
            <p>I'm <b>Luthfi Aji Wicaksono</b> I was born in Ngawi, East Java, Indonesia and I was 20 years old on 6 September 2018.
             My motto is <em>Delay fast result for long-term succes.</em> I'm a junior front-end developer.
             I'm working with React Js and Bootstrap as my main framework, and I still learn about both of them until now.
             And of course I will also learn about the back-end of a website and become a full-stack developer, In Shaa Allah.

           </p>
          </div>
        </div>

        <div className="row hr-tab">
          <div className="col-md-12 mx-auto ">
            <p className="hr-tab text-center">.......</p>
          </div>
        </div>

        <div className="overlay-1">
          <div className="row mt p-2">
            <div className="col-md-4 offset-md-2 mt-4">
              <h3>Personal Details</h3>
              <table className="personal mb-4">
                <tr>
                  <th>Name</th>
                  <td>: Luthfi Aji Wicaksono</td>
                </tr>
                <tr>
                  <th>Address</th>
                  <td>: Tegalsari Village rt/07 rw/08 no. 18, Ngawi-East Java </td>
                </tr>
                <tr>
                  <th>Place, Date of Birth</th>
                  <td>: Ngawi, September 6th, 1998</td>
                </tr>
                <tr>
                  <th>Gender</th>
                  <td>: Male</td>
                </tr>
                <tr>
                  <th>Marital Status</th>
                  <td>: Single</td>
                </tr>
                <tr>
                  <th>Nationality</th>
                  <td>: Indonesia</td>
                </tr>
                <tr>
                  <th>Religion</th>
                  <td>: Moslem</td>
                </tr>
                <tr>
                  <th>Cellphone</th>
                  <td>: +6282246854054</td>
                </tr>
                <tr>
                  <th>Hobby</th>
                  <td>: Coding, Writing, Reading novels, Reciting Quran</td>
                </tr>
              </table>
            </div>
            <div className="col-md-5 offset-md-1 mt-4">
              <h3>Education</h3>
              <ul className="personal">
                <li>SDS Al-Mukhlisin West Jakarta</li>
                <li>SMPN 105 West Jakarta</li>
                <li>SMAN 56 West Jakarta (1 semester)</li>
                <li>SMAN 1 Ngawi</li>
                <li>Ma'had Madinatul Quran Bogor</li>
                <li>Ma'had Riyadhus Shalihin Banyumas</li>
              </ul>
              <h3 className="mt-4">Languange Proficiency</h3>
              <ul className="personal">
                <li>Indonesia : Native</li>
                <li>English   : Passive</li>
                <li>Arabic    : Good</li>
              </ul>
              <h3 className="mt-4">Interest</h3>
              <ul className="personal">
                <li>Photography</li>
                <li>Jogging</li>
                <li>Writing novels</li>
              </ul>
            </div>
          </div>
        </div>

        <div className="row hr-tab">
          <div className="col-md-12 mx-auto ">
            <p className="hr-tab text-center">.......</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Profile;
