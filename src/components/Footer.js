import React from 'react';
import '../App.js';

class Footer extends React.Component {
  render() {
    return (
      <footer className="bd-footer bg-light border border-white mt-5">
        <div className="container-fluid mt-3 p-3">
          <div className="row">

            <div className="col-md-4 order-md-3">
              <ul className="bd-footer-link">
                <li><a href="https://www.instagram.com/lawdev_/" target="_blank" rel="noopener noreferrer"><i className="fab fa-instagram"></i></a></li>
                <li><a href="https://www.facebook.com/Luthfi.Mahmudi" target="_blank" rel="noopener noreferrer"><i className="fab fa-facebook-square"></i></a></li>
                <li><a href="https://gitlab.com/luthfiajiw" target="_blank" rel="noopener noreferrer"><i className="fab fa-gitlab"></i></a></li>
                <li><a href="https://www.linkedin.com/in/luthfi-aji-w-63343b16a/" target="_blank" rel="noopener noreferrer"><i className="fab fa-linkedin"></i></a></li>
              </ul>
            </div>

            <div className="col-md-4 order-md-1">
              <p>Let's have some discussion or just a <br/> chit-chat with me</p>
              <p className="email"><a href="mailto:luthfi.ajiw@gmail.com">luthfi.ajiw@gmail.com</a></p>
            </div>

            <div className="col-md-4 order-md-2"><p className="text-center">&copy; 2018</p></div>
          </div>
        </div>
      </footer>
    );
  }
}


export default Footer;
