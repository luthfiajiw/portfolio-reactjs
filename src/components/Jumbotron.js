import React from 'react';
import '../App.css';
import png from './1.png';

class Profile extends React.Component {
  render() {
    return(
      <div className="container-fluid con-jumbo">
        <div className="row align-items-center col-jumbo pt-5">
          <div className="col-6 col-md-6 mx-auto order-md-2">
            <img className="img-fluid mb-md-0 mb-3" src={png}/>
          </div>
          <div className="col-md-6 order-md-1 text-center text-md-left pt-2">
            <h1 className="h1-jumbo">Hello !</h1>
            <br/>
            <p className="lead">I am not just typing, but also thinking.</p>
            <p className="lead">Nothing is difficult if you enjoy it.</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Profile;
