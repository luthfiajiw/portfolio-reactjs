import React from 'react';
import '../App.css';

class Navbar extends React.Component {
  render() {
    return(
      <nav className="d-flex navbar nav-light pt-3 bg-light sticky-top">
        <a className="navbar-brand" href="/">&nbsp; LAW</a>

          <ul className="nav justify-content-center">
            <li className="nav-item">
              <a className="nav-link" href="#profile">Profile</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#about">Skill</a>
            </li>
          </ul>
      </nav>
    );
  }
}

export default Navbar;
